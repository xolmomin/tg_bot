import base64

import requests
import telebot
from django.core.files.base import ContentFile
from rest_framework.response import Response
from rest_framework.views import APIView
from telebot import apihelper
from telebot.types import ReplyKeyboardMarkup, KeyboardButton, InlineKeyboardButton, \
    InlineKeyboardMarkup

from app.models import TgUser as User, Category, Product, TgUser, Order

BOT_NAME = 'desscrbot'
token = '1368132434:AAEsWk4lJ3_MFKmTpFl6vDMdBDwHI7q_XYo'

CLICK_SERVICE_ID = 13751
CLICK_MERCHANT_ID = 9395

PAYME_MERCHANT_ID = '5d9f58c77a1066cc7fe8fa8f'
PAYME_KEY_1 = 'ucs4Ni&O6OyRyMJf3pUKoYm7y??#PoKSxO@T'

bot = telebot.TeleBot(token)
apihelper.ENABLE_MIDDLEWARE = True


class UpdateMagazinBot(APIView):
    def post(self, request):
        json_string = request.body.decode('UTF-8')
        update = telebot.types.Update.de_json(json_string)
        bot.process_new_updates([update])
        return Response({'code': 200})


TRANSLATIONS = {
    'Выбор языка': {
        'ru': 'Выбор языка',
        'uz': 'Tilni tanlang'
    },
    'Меню': {
        'ru': 'Меню',
        'uz': 'Menyu'
    },
    'Купить полис': {
        'ru': 'Купить полис',
        'uz': 'Polis sotib olish'
    },
    '📞СОС (Страховой случай)': {
        'ru': 'СОС (Страховой случай)',
        'uz': 'SOS'
    },
    'Страхование имущества': {
        'ru': 'Страхование имущества',
        'uz': 'Страхование имущества'
    },
    'Страхования КАСКО': {
        'ru': 'Страхования КАСКО',
        'uz': 'Страхования КАСКО'
    },
    'Страхование от несчастных случаев': {
        'ru': 'Страхование от несчастных случаев',
        'uz': 'Страхование от несчастных случаев'
    }
}

LANG = 'ru'


def _(string):
    return TRANSLATIONS[string][LANG]


@bot.middleware_handler(update_types=['message', 'callback_query'])
def activate_language(bot_instance, msg):
    if User.objects.filter(pk=msg.from_user.id).exists():
        user = User.objects.get(pk=msg.from_user.id)
        msg.from_user.language_code = user.language
        print(user.step)


@bot.message_handler(commands=['start'])
def start(msg):
    user, created = User.objects.get_or_create(pk=msg.from_user.id)
    if created:
        name = msg.from_user.first_name
        user.first_name = msg.from_user.first_name
        user.last_name = msg.from_user.last_name
        user.username = msg.from_user.username
        text = f'Salom {name}! \n' \
               'Iltimos tilni tanlang! \n\n' \
               f'Привет {name}! \n' \
               'Пожалуйста выберите язык!'
        markup = InlineKeyboardMarkup()
        markup.row(InlineKeyboardButton('🇺🇿O\'zbekcha', callback_data='lang_uz'))
        markup.row(InlineKeyboardButton('🇷🇺 Русский', callback_data='lang_ru'))
        bot.send_message(msg.from_user.id, text, reply_markup=markup)
    else:
        rkm = ReplyKeyboardMarkup(resize_keyboard=True)
        rkm.add(KeyboardButton(_('Купить полис')))
        rkm.add(KeyboardButton(_('📞СОС (Страховой случай)')))
        bot.send_message(msg.from_user.id, _('Меню'), reply_markup=rkm)
    user.step = 'menu'
    user.save()


@bot.callback_query_handler(func=lambda c: True)
def callback(call):
    user_id = call.from_user.id
    if call.data.startswith('lang'):
        global LANG
        if call.data == 'lang_uz':
            User.objects.filter(pk=user_id).update(language='uz')
            LANG = 'uz'
        elif call.data == 'lang_ru':
            User.objects.filter(pk=user_id).update(language='ru')
            LANG = 'ru'
        rkm = ReplyKeyboardMarkup(resize_keyboard=True)
        rkm.add(KeyboardButton(_('Купить полис')))
        rkm.add(KeyboardButton(_('📞СОС (Страховой случай)')))
        bot.send_message(user_id, _('Меню'), reply_markup=rkm)
    elif call.data.startswith('info_'):
        product = Product.objects.get(id=call.data.replace('info_', ''))
        markup = InlineKeyboardMarkup()
        markup.add(InlineKeyboardButton('next', callback_data='buy_product_{}'.format(product.id)))
        user = TgUser.objects.get(pk=user_id)
        user.step = 'enter_name'
        user.save()
        bot.send_message(
            user_id,
            f"*{product.safe_translation_getter('name')}*\n"
            f"{'-' * 20}\n"
            f"{product.safe_translation_getter('description')}",
            reply_markup=markup,
            parse_mode='markdown'
        )
    elif call.data.startswith('buy_product_'):
        product = Product.objects.get(id=call.data.replace('buy_product_', ''))
        user = TgUser.objects.get(pk=user_id)
        Order.objects.create(user=user, product=product, price=product.price)
        user.step = 'enter_name'
        user.save()
        bot.send_message(user_id, 'Введите Ф. И. О. полностью')
    elif call.data == 'onlayn_tolov':

        order = Order.objects.filter(user_id=user_id).order_by('-created_at').first()
        order_id = 1
        amount = order.product.price
        click_url = f"https://my.click.uz/services/pay?service_id={CLICK_SERVICE_ID}&merchant_id={CLICK_MERCHANT_ID}&amount={amount}&transaction_param={order_id}&return_url=https://t.me/s/{BOT_NAME}"

        params = f"m={PAYME_MERCHANT_ID};ac.order_id={order_id};a={amount};c=https://t.me/s/{BOT_NAME}"
        encode_params = base64.b64encode(params.encode("utf-8"))
        encode_params = str(encode_params, 'utf-8')
        payme_url = f"https://checkout.paycom.uz/{encode_params}"

        markup = InlineKeyboardMarkup()
        markup.add(InlineKeyboardButton("Click", callback_data='pay_click', url=click_url))
        markup.add(InlineKeyboardButton("Payme", callback_data='pay_payme', url=payme_url))
        user = TgUser.objects.get(pk=user_id)
        bot.send_message(user_id, 'tolov turi', reply_markup=markup)
    else:
        pass


@bot.message_handler(content_types=['text'])
def text_message(message):
    user = TgUser.objects.get(pk=message.from_user.id)
    switcher = {
        'menu': menu,
        'product_list': product_list,
        'enter_name': enter_name,
        'address_of_insurance': address_of_insurance,
        'phone_number': phone_number,
        # 'photo_of_passport': photo_of_passport,
        'offer': offer,
        # 'payment_method': payment_method,
        # 'paying': paying,
        # 'retrieve_way': retrieve_way,
        # 'retrieve_way_selection': retrieve_way_selection,
        # 'retrieving_send_policy_via_email': retrieving_send_policy_via_email,
        # 'send_policy_retrieve_location': send_policy_retrieve_location,
        # 'send_policy_send_location': send_policy_send_location
    }
    func = switcher.get(user.step, lambda: start(message))
    func(message)


def menu(message):
    user = TgUser.objects.get(pk=message.from_user.id)
    if message.text == _('Купить полис'):
        rkm = ReplyKeyboardMarkup(resize_keyboard=True)
        text = 'Выберите категорию'
        categories = Category.objects.all()
        for category in categories:
            rkm.add(KeyboardButton(category.name))
        bot.send_message(message.from_user.id, text, reply_markup=rkm)
    user.step = 'product_list'
    user.save()


def product_list(message):
    user = TgUser.objects.get(pk=message.from_user.id)
    products = Product.objects.filter(category__translations__name=message.text)
    for product in products:
        markup = InlineKeyboardMarkup()
        markup.add(InlineKeyboardButton(product.name, callback_data='info_{}'.format(product.id)))
        bot.send_photo(
            chat_id=message.chat.id,
            photo=product.image.read(),
            caption=product.name,
            reply_markup=markup,
        )
        user.step = 'name_details'
        user.save()
    if not products:
        bot.send_message(message.from_user.id, 'not found!')


def enter_name(message):
    user = TgUser.objects.get(pk=message.from_user.id)
    user.step = 'address_of_insurance'
    user.save()
    order = Order.objects.filter(user=user).last()
    order.full_name = message.text
    order.save()
    bot.send_message(message.from_user.id, 'Введите адрес страховки')


def address_of_insurance(message):
    user = TgUser.objects.get(pk=message.from_user.id)
    user.step = 'phone_number'
    user.save()
    order = Order.objects.filter(user=user).last()
    order.address = message.text
    order.save()
    rkm = ReplyKeyboardMarkup(resize_keyboard=True)
    rkm.add(KeyboardButton('send phone', request_contact=True))
    bot.send_message(message.from_user.id, 'Отправьте свой номер телефона', reply_markup=rkm)


def phone_number(message):
    user = TgUser.objects.get(pk=message.from_user.id)
    user.step = 'phone_number'
    user.save()
    order = Order.objects.filter(user=user).last()
    order.address = message.text
    order.save()
    bot.send_message(message.from_user.id, 'Введите адрес страховки')


@bot.message_handler(content_types=['contact'])
def read_contact(message):
    user = TgUser.objects.get(pk=message.from_user.id)
    user.step = 'photo_of_passport'
    user.phone = message.contact.phone_number
    user.save()
    order = Order.objects.filter(user=user).last()
    order.phone_number = message.contact.phone_number
    order.save()
    bot.send_message(message.from_user.id, 'Отправьте изображения вашего паспорта')


@bot.message_handler(content_types=['photo', 'document'])
def read_location(msg):
    if msg.document or msg.photo:
        if msg.photo:
            file_id = msg.photo[0].file_id
        elif msg.document:
            file_id = msg.document.file_id
        else:
            file_id = None
        if file_id:
            user = TgUser.objects.get(pk=msg.from_user.id)
            file_info = bot.get_file(file_id)
            file_name = file_info.file_path.split('/')[1]
            order = Order.objects.filter(user=msg.from_user.id).last()
            file = requests.get(f'https://api.telegram.org/file/bot{token}/{file_info.file_path}')
            order.passport.save(file_name, ContentFile(file.content))
            user.passport.save(file_name, ContentFile(file.content))
            user.step = 'offer'
            user.save()
            order.save()
            rkm = ReplyKeyboardMarkup(resize_keyboard=True)
            rkm.add(KeyboardButton('Принимаю'))
            bot.send_message(msg.from_user.id, 'Оферта согласно продукту', reply_markup=rkm)
    else:
        bot.send_message(msg.from_user.id, 'Отправьте изображения вашего паспорта again')


def offer(message):
    text = 'To\'lov turini tanlang!!!'
    reply_markup = InlineKeyboardMarkup()
    reply_markup.add(InlineKeyboardButton("Onlayn", callback_data='onlayn_tolov'))
    reply_markup.add(InlineKeyboardButton("Offline", callback_data='offlayn_tolov'))
    bot.send_message(message.from_user.id, text, reply_markup=reply_markup)
