from django.contrib import admin
from parler.admin import TranslatableAdmin

from app.models import Category, Product, Service, Office, TgUser, Order, PaycomTransaction


class CategoryAdmin(TranslatableAdmin):
    class Meta:
        model = Category
        fields = [
            'name',
            'offer'
        ]


class ProductAdmin(TranslatableAdmin):
    class Meta:
        model = Product
        fields = [
            'name',
            'category',
            'price',
        ]


class ServiceAdmin(TranslatableAdmin):
    class Meta:
        model = Service
        fields = [
            'name',
        ]


class OfficeAdmin(TranslatableAdmin):
    class Meta:
        model = Office
        fields = [
            'name',
            'address',
            'phone_number',
        ]


admin.site.register(TgUser)
admin.site.register(Order)
admin.site.register(Category, CategoryAdmin)
admin.site.register(Product, ProductAdmin)
admin.site.register(Service, ServiceAdmin)
admin.site.register(Office, OfficeAdmin)
admin.site.register(PaycomTransaction)
