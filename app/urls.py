from django.urls import path, include
from django.views.decorators.csrf import csrf_exempt

from app.bot import UpdateMagazinBot

urlpatterns = [
    path('bot', csrf_exempt(UpdateMagazinBot.as_view())),
]
