from autoslug import AutoSlugField
from django.db import models
from django.utils.translation import ugettext as _
from parler.models import TranslatableModel, TranslatedFields
from location_field.models.plain import PlainLocationField


class TgUser(models.Model):
    username = models.CharField(max_length=255, null=True, blank=True)
    first_name = models.CharField(max_length=500, null=True, blank=True)
    last_name = models.CharField(max_length=500, null=True, blank=True)
    language = models.CharField(max_length=15, null=True, choices=(('uz', "O'zbekcha"), ('ru', "Русский")))
    phone = models.CharField(max_length=15, null=True, blank=True)
    passport = models.ImageField(upload_to="uploads/user_passports/%Y/%m/", null=True, blank=True)
    created_at = models.DateTimeField(auto_now=True)
    step = models.CharField(max_length=255, default='welcome', choices=(
        ('language', "Language"),
        ('menu', "Menu"),
        ('product_list', "Product list"),
        ('product_info', "Product info"),
        ('enter_name', "Enter name"),
        ('address_of_insurance', "Address"),
        ('phone_number', "Phone number"),
        ('photo_of_passport', "Passport photo"),
        ('offer', "Offer"),
        ('payment_method', "Payment method"),
        ('paying', "Paying"),
        ('retrieve_way', "Retrieve way"),
        ('retrieve_way_selection', "Retrieve way selection"),
        ('retrieving_send_policy_via_email', 'Send policy to email'),
        ('send_policy_retrieve_location', 'Send policy retrieve location'),
        ('send_policy_send_location', 'Send policy send location'),
    ))

    def __str__(self):
        return "{} / {}".format(self.first_name, self.last_name)


class Category(TranslatableModel):
    translations = TranslatedFields(
        name=models.CharField(max_length=255),
        offer=models.TextField(null=True, blank=True)
    )

    def __str__(self):
        return str(self.safe_translation_getter('name'))


class Product(TranslatableModel):
    translations = TranslatedFields(
        name=models.CharField(max_length=255),
        image=models.ImageField(upload_to="uploads/product/"),
        description=models.TextField(null=True)
    )
    category = models.ForeignKey(Category, on_delete=models.CASCADE, null=True)
    slug = AutoSlugField(populate_from='name', always_update=True, null=True)
    price = models.IntegerField(null=True)
    created_time = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.safe_translation_getter('name'))


class Service(TranslatableModel):
    translations = TranslatedFields(
        name=models.CharField(max_length=255),
    )

    def __str__(self):
        return self.safe_translation_getter('name')


class Office(TranslatableModel):
    translations = TranslatedFields(
        name=models.CharField(max_length=255),
        address=models.CharField(max_length=500),
    )
    phone_number = models.CharField(max_length=255)

    location = PlainLocationField(based_fields=['city'], zoom=7, null=True)

    def __str__(self):
        return self.safe_translation_getter('name')


class Order(models.Model):
    PAYMENT_PROVIDER_PAYME = 'payme'
    PAYMENT_PROVIDER_CLICK = 'click'
    PAYMENT_PROVIDERS = (
        (PAYMENT_PROVIDER_PAYME, "PayMe"),
        (PAYMENT_PROVIDER_CLICK, "Click"),
    )

    PAYMENT_METHOD_ONLINE = 'online'
    PAYMENT_METHOD_CASH = 'cash'
    PAYMENT_METHODS = (
        (PAYMENT_METHOD_ONLINE, _("Online")),
        (PAYMENT_METHOD_CASH, _("Cash")),
    )

    RETRIEVE_WAY_ONLINE = 'online'
    RETRIEVE_WAY_DELIVERY = 'delivery'
    RETRIEVE_WAY_PICKUP = 'pickup'
    RETRIEVE_WAYS = (
        (RETRIEVE_WAY_ONLINE, "ONLINE"),
        (RETRIEVE_WAY_DELIVERY, "DELIVERY"),
        (RETRIEVE_WAY_PICKUP, "PICKUP"),
    )

    STATUS_PENDING = 'pending'
    STATUS_ON_PROCESS = 'on_process'
    STATUS_RECEIVED = 'received'
    STATUS_COMPLETED = 'completed'
    STATUS_CANCELLED = 'cancelled'
    STATUS_CHOICES = (
        (STATUS_PENDING, _("PENDING")),
        (STATUS_ON_PROCESS, _("ON_PROCESS")),
        (STATUS_RECEIVED, _("RECEIVED")),
        (STATUS_COMPLETED, _("COMPLETED")),
        (STATUS_CANCELLED, _("CANCELLED"))
    )

    user = models.ForeignKey(TgUser, on_delete=models.SET_NULL, null=True)
    full_name = models.CharField(max_length=255, null=True)
    phone_number = models.CharField(max_length=255, null=True)
    status = models.CharField(max_length=255, default=STATUS_PENDING)
    price = models.IntegerField(null=True, blank=True)
    product = models.ForeignKey(Product, on_delete=models.SET_NULL, null=True)
    address = models.CharField(max_length=255, null=True, blank=True)
    passport = models.ImageField(null=True, blank=True, upload_to="uploads/user_passports/%y/%m/")
    payment_method = models.CharField(max_length=255, choices=PAYMENT_METHODS, null=True)
    payment_provider = models.CharField(max_length=255, choices=PAYMENT_PROVIDERS, null=True)
    retrieve_type = models.CharField(max_length=255, choices=RETRIEVE_WAYS, null=True, blank=True)
    email = models.EmailField(null=True, blank=True)
    lat = models.FloatField(null=True, blank=True)
    lng = models.FloatField(null=True, blank=True)
    delivery_place = models.TextField(null=True, blank=True)
    is_payed = models.BooleanField(default=False, null=True)
    click_payment_request = models.ForeignKey('ClickPaymentRequest', on_delete=models.CASCADE, null=True, blank=True)

    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.id)


class PaycomTransaction(models.Model):
    paycom_transaction_id = models.CharField(max_length=255, null=False, unique=True)
    paycom_time = models.CharField(max_length=15)
    paycom_time_datetime = models.DateTimeField(null=False)
    create_time = models.DateTimeField(null=False)
    perform_time = models.DateTimeField(null=True, blank=True)
    cancel_time = models.DateTimeField(null=True, blank=True)
    amount = models.CharField(max_length=50, null=False)
    state = models.IntegerField(null=False)
    reason = models.IntegerField(null=True, blank=True)
    receivers = models.TextField(null=True, blank=True)

    order = models.ForeignKey(Order, on_delete=models.CASCADE)

    def __str__(self):
        return "Order id {}".format(self.order.pk)


class ClickPaymentRequest(models.Model):
    user = models.ForeignKey(TgUser, on_delete=models.SET_NULL, null=True)
    phone_number = models.CharField(max_length=255)
    is_success = models.BooleanField(default=False)
    date = models.DateTimeField(auto_now_add=True)
    error_code = models.PositiveIntegerField()
    error_note = models.CharField(max_length=1000)
    invoice_id = models.IntegerField()

    def __str__(self):
        return str(self.id)
        # return f"{ self.user.first_name } ({ self.phone_number }) @ { self.date }"
